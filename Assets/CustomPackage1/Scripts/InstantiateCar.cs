﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateCar : MonoBehaviour
{
    public GameObject carPrefab;
    public Transform instPosition;

    // Start is called before the first frame update
    void Start()
    {
        if (carPrefab == null)
            Debug.LogError("no prefab assigned");
    }

    public void InstantiateCarMethod()
    {
        GameObject.Instantiate(carPrefab, instPosition.position, Quaternion.identity, null);
    }
}
